#!/bin/bash

#Processing times: 
#real    17m37.850s
#user    3m10.957s
#sys     3m20.664s

#real    13m43.878s
#user    2m47.898s
#sys     2m48.602s

file1="unique_tracks.txt"
file2="triplets_sample_20p.txt"
if ! [ -f "$file1" ]
then
	echo "Nie znaleziono $file1"
	exit 1
fi
if ! [ -f "$file2" ]
then
	echo "Nie znaleziono $file2"
	exit 1
fi

echo "Rozpoczynam zmiane separatora na przecinek w pliku $file1 ..."
cat $file1 | sed "s/<SEP>/,/g" > "$file1-transformed"
echo "Wygenerowano plik $file1-transformed"

echo "Rozpoczynam zmiane separatora na przecinek w pliku $file2 ..."
cat $file2 | sed "s/<SEP>/,/g" > "$file2-transformed"
echo "Wygenerowano plik $file2-transformed"


echo "Rozpoczynam generowanie pliku z utworami..."
join -t "," -1 2 -2 2 -o 1.1,2.1,2.2,2.3,2.4 <(cat "$file1-transformed" | cut -d "," -f2 | sort -u| nl -s "," -w1) <( cat "$file1-transformed" | sort -t "," -u -k2,2) > songs
echo "Plik songs wygenerowany."

echo "Rozpoczynam generowanie pliku z datami..."
cat "$file2-transformed" | cut -d "," -f3 | sort -n | sed 's/^/@/' | date +"%d,%m,%Y,%F" -f - | uniq | nl -s "," -w1 > dates
echo "Plik dates wygenerowany"

echo "Rozpoczynam generowanie pliku z uzytkownikami..."
cat "$file2-transformed" | cut -d "," -f1 | sort -u | nl -s "," -w1 > users
echo "Plik users wygenerowany"

echo "Rozpoczynam generowanie pliku z odsluchaniami..."
join -t "," -1 1 -2 2 -o 2.1,1.2,1.3 <(cat "$file2-transformed" | sort -t "," -k1,1)  <(cat users) > listen_users
join -t "," -1 2 -2 3 -o 1.1,2.1,1.3 <(cat listen_users | sort -t "," -k2,2)  <(cat songs) > listen_songs
join -t "," -1 2 -2 5 -o 1.1,2.1 <(cat "$file2-transformed" | cut -d "," -f3 | sort -n | sed 's/^/@/' | date +%F -f - | nl -s "," -w1) <(cat dates) > listen_dates

join -t "," -1 1 -2 1 -o 1.2,1.3,2.2 <(cat listen_songs | sort -t "," -k3n,3 | nl -s "," -w1) <(cat listen_dates) > listen

echo "Rozpoczynam sprzatanie plikow..."
rm "$file1-transformed"
rm "$file2-transformed"
rm listen_users
rm listen_songs
rm listen_dates
echo "Pliki zostaly posprzatane"

echo "Zmiana struktury danych zakonczona sukcesem"
