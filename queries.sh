#!/bin/bash

#Processing time:
#real    4m51.713s
#user    1m19.235s
#sys     0m11.567s

echo "1. Ranking popularnosci utworow:"
join -t "," -1 2 -2 1 -o 2.4,1.1 <(cat listen | cut -d "," -f2 | sort | uniq -c | sed 's/^ *//;s/ /,/' | sort -t "," -k1rn,1 | head | sort -t "," -k2,2) <(cat songs | sort -t "," -k1,1) | sort -t "," -k3nr,3

echo "2. Ranking uzytkownikow ze wzgledu na najwiekszą liczbe odsluchanych unikalnych utworow:"
join -t "," -1 2 -2 1 -o 2.2,1.1 <(cat listen | sort -t "," -u -k1n,2n | cut -d "," -f1 | uniq -c | sed 's/^ *//;s/ /,/' | sort -t "," -k1rn,1 | head | sort -t "," -k2,2) <(cat users | sort -t "," -k1,1) | sort -t "," -k2rn,2 

echo "3. Artysta z najwieksza liczba odsluchan:"
join -t "," -1 1 -2 1 -o 1.2 <(cat songs | cut -d "," -f1,4 | sort -t "," -k1,1) <(cat listen | cut -d "," -f2 | sort -k1,1) | sort -k1,1 | uniq -c | sed 's/^ *//;s/ /,/' | sort -t "," -k1rn,1 | head -1 

echo "4. Sumaryczna liczba odsluchan w podziale na poszczegolne miesiace:"
join -t "," -1 1 -2 1 -o 2.3 <(cat listen | cut -d ',' -f3 | sort -k1n,1) <(cat dates | sort -t "," -k1n,1) | sort -k1n,1 | uniq -c | sed 's/^ *//;s/ /,/'

echo "5. Uzytkownicy ktorzy odsluchali trzy najbardziej popularne utwory Queen:"
if ! [ -f "query5" ]
then
	join -t "," -1 1 -2 2 -o 2.2 <(cat songs | grep ",Queen," | sort -t "," -k1) <(cat listen | sort -t "," -k2,2) | uniq -c | sed "s/^ *//;s/ /,/" | sort -t "," -k1rn,1 | head -3 | cut -d "," -f2 > query5
fi

join -t "," -1 2 -2 1 -o 2.2 <(cat listen | grep -f query5 | cut -d "," -f1,2 | sort -t "," -u -k1,2 | cut -d "," -f1 | sort | uniq -c | sed 's/^ *//;s/ /,/' | grep "3," | sort -t "," -k2,2) <(cat users | sort -t "," -k1,1) | sort

